��    u      �              l     m     }     �     �     �     �     �     �  4   �  .         >  !   _     �     �     �     �     �  !   �     �     	     &	     ;	     A	     N	     V	  !   \	     ~	     �	  	   �	     �	     �	     �	     �	     �	     �	     
     
     %
     *
  	   <
     F
     L
     T
     ]
     b
     }
  o   �
     �
  "        /     E     a          �     �     �     �     �     �  	   �     �  "   �     
          &  3   4  4   h     �     �  )   �     �     �     �  
   �     �               &     /     =     R  
   c  $   n     �     �     �     �  '   �                    '  
   B     M     S     Z     h     y     �     �     �     �     �     �     �     �  
   �  	   �     �     �     �                    "     '  �  -     �     �     �     �  
   �                 *   '  *   R     }     �     �     �     �     �     �                )     ?     U     \     i     p     w     �     �     �  	   �  	   �     �     �     �     �     �               #     6     C     J     Q     X     _     x  6   �     �     �     �     �               +     >     E     L     Y     `     g  	   t     ~     �     �     �  !   �  !   �  	          -     	   >     H     O     V     f     s     �     �     �     �     �     �     �     �          )     0     @     \     c     j     w     �     �     �     �     �     �     �     �     �     �     �     �  	   �     	  	              -     4     ;     K     X     e     l     p    -- LT Maker %s &About &New Project... &Open Project... &Preferences... &Quit &Save Project Advantage versus Auto-assign combat animation with the same unique ID Auto-assign map sprite with the same unique ID Base damage with magical weapons Base damage with physical weapons Base defense Base heal formula Base hit rate Base magical defense Boss Bonus Stats Chance to avoid an enemy's attack Check for updates... Choose Combat Animation... Choose Map Sprite... Class Class Skills Classes Color Column within Info Menu in engine Combat Background Combat Platform Type Constants Crit chance Database Description Difficulty Mode Difficulty Modes Disadvantage versus Display Name Doubling threshold Edit Enemy Bonus Stats Equations Extra Faction Factions File Flat damage bonus on crits Force Melee Anim Force this weapontype to use the melee animation at melee range, even with a ranged weapon. Ex. Javelins in GBA Growth method Import classes from class_info.xml Import items from csv Import items from items.xml Import skills from status.xml Import units from csv Import units from units.xml Item Items Leader Unit Level Limits Max Level Maximum Maximum weight that can be rescued Minimap Type Movement Costs Movement Type Multiplicative damage bonus on crits, after def/res Multiplicative damage bonus on crits, before def/res No tag None Offsets weight speed reduction from items One way? Parties Party Permadeath Personal Skills Personal Tags Player Bonus Stats Position Promotes From Promotion Options... Range and Points Rank Bonus Rank Requirements & Personal Bonuses Reduces enemy's crit chance Remove Unused Resources Resource Save Project As... Second unit gives bonuses to first unit Skill Skills Starting Items Starting Weapon Experience Stat Types Stats Status Steal ability Steal resistance Support Constants Supports Tags Terrain Test Tier Total HP Total movement Translations Turns Into Unique ID Unit Units Weapon Experience Weapon Type Weapon Types hidden left right Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2022-11-29 23:09+0800
PO-Revision-Date: 2022-11-17 22:26-0600
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: zh_Hans_CN
Language-Team: zh_Hans_CN <LL@li.org>
Plural-Forms: nplurals=1; plural=0;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.11.0
  -- LT 编辑器 %s 关于 新建项目 打开项目 &首选项 退出 保存项目 有利相性 根据唯一标识自动匹配战斗动画 根据唯一标识自动匹配地图形象 魔法武器的基础伤害 物理武器的基础伤害 基础防御 基础回复量 基础命中率 基础魔防 BOSS属性增益 回避对方攻击的概率 检查更新 选择战斗动画... 选择地图形象... 职业 职业技能 职业 颜色 在状态界面的位置 战斗背景 战斗平台方块 游戏设置 必杀率 数据库 描述 游戏难度 游戏难度 不利相性 显示名称 追击攻速差 编辑 敌方属性增益 计算公式 杂项 徽标 徽标 文件 必杀时的伤害增益 锁近战动画 近战时强制播放近战动画。例：GBA的投枪 成长模式 从XML导入职业 从CSV导入道具 从XML导入道具 从XML导入技能 从CSV导入角色 从XML导入角色 道具 道具 领导角色 等级 限制 最大等级 最大值 最高能救出的重量 地图图标 地形阻碍 移动类型 必杀倍率（防御结算后） 必杀倍率（防御结算前） 无标签 无 人物身型的大小，与救出指令有关 单向？ 阵营 阵营 不死鸟选项 个人技能 角色标签 玩家属性增益 位置 从...转职 转职设定... 范围、亲密度 熟练度加成 亲密度要求&支援加成 减少对方必杀的概率 移除未使用资源 资源 另存为项目 仅第一人可获得增益 技能 技能 初始道具 初始武器熟练度 状态参数 状态参数 状态 偷盗能力 防偷能力 游戏设置 支援 标签 地形 运行 位阶 血量 移动力 说明文字 转职为 唯一标识 角色 角色 武器熟练度 武器类型 武器类型 隐藏 左 右 